#pragma once
#include "Class.h"
#include <vector>

class Mage : public Class{
protected:
	float runeUp, runeCd, runeStacks;
public:
	std::vector<float> results;
public:
	Mage();
	Mage(int iter, int tar, float time, std::vector<int> tal, float intel, int crit, int haste, int mastery, int versatility);
	~Mage() {};
	bool runeOfPower();
	bool ring();
};
