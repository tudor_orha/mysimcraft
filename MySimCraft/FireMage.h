#pragma once
#include "Mage.h"

class FireMage : public Mage{
private:
	int pyroProc;
	int EnhancedPyrotechnics;
	float fireBlastCd,dragonsBreathCd,livingBombCd;
	float combustCd, combustUp;
public:
	FireMage();
	FireMage(int iter, int tar, float time, std::vector<int> tal, float intel, int crit, int haste, int mastery, int vers);
	~FireMage() {};
	void fireball();
	void pyroblast();
	bool fireBlast();
	bool dragonsBreath();
	bool livingBomb();
	bool combustion();
	double sim() override;
	//std::vector<double> plotIntel();
	//std::vector<double> plotCrit();
	//std::vector<double> plotHaste();
	//std::vector<double> plotMastery();
	//std::vector<double> plotVersatility();
};
