#include "mysimcraft.h"
#include <ctime>

MySimCraft::MySimCraft(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	connect(this->ui.simButton, SIGNAL(clicked()), this, SLOT(run()));
	//connect(0, SIGNAL(signalProgress(int)), this->ui.progressBar, SLOT(setValue(int)));
	this->ui.progressBar->setValue(0);
}

void MySimCraft::run()
{
	//FireMage x{ 10000,1,60,{ 2,0,1,0,1,0,0 }, 3652, 1194, 1940, 1356, 228};
	FireMage x{ 10000,1,300,{ 2,0,1,0,1,0,0 }, 8838, 2512, 1532, 2801, 228 };
	Priest y{ 10000,1,300,{ 2,0,1,0,1,0,0 }, 8838, 2512, 1532, 2801, 228 };
	c = &x;
	iterations = 15000;
	//this->ui.spinBox->setValue(1000);
	iterations = this->ui.spinBox->value();
	simulate();
	if (ui.plotCheckBox->isChecked() == 1) plot();
}

void MySimCraft::simulate() {
	double damagePerSecond = 0;
	int iters = iterations;
	srand(time(NULL));
	while (iters > 0) {
		damagePerSecond += c->sim() / iterations;
		iters--;
		this->ui.progressBar->setValue(100 - (iters / (iterations / 100.0)));
	}
	this->ui.dps->setText(QString::fromStdString(std::to_string(damagePerSecond)));
}

void MySimCraft::plot()
{
	ui.intellectList->clear();
	ui.critList->clear();
	ui.hasteList->clear();
	ui.masteryList->clear();
	ui.versatilityList->clear();

	std::vector<double> aux;
	std::vector<double> primary = c->plotPrimary();

	for (int j = 0; j < 1000; j++) {
		primary[i] += c->plotPrimary();
	}


	for (int i = 0; i <= 40; i++) {
		QString item = QString::fromStdString(std::to_string(i) + " : " + std::to_string(primary[i]));
		this->ui.intellectList->addItem(item);
	}
	
	//for (int i = 0; i < f.results.size(); i++) {
	//	QString item = QString::fromStdString(std::to_string(f.results[i]));
	//	this->ui.intellectList->addItem(item);
	//	this->ui.plotCheckBox->isChecked();
	//}
	
	//::vector<double> intel = f.plotIntel();
	//std::vector<double> crit = f.plotCrit();
	//std::vector<double> haste = f.plotHaste();
	//std::vector<double> mastery = f.plotMastery();
	//std::vector<double> vers = f.plotVersatility();
	
	//for (int i = 0; i < intel.size(); i++) {

		//QString item = QString::fromStdString(std::to_string(i) + " : " + std::to_string(intel[i]));
		//this->ui.intellectList->addItem(item);
		//item = QString::fromStdString(std::to_string(i) + " : " + std::to_string(crit[i]));
		//this->ui.critList->addItem(item);
		//item = QString::fromStdString(std::to_string(i) + " : " + std::to_string(haste[i]));
		//this->ui.hasteList->addItem(item);
		//item = QString::fromStdString(std::to_string(i) + " : " + std::to_string(mastery[i]));
		//this->ui.masteryList->addItem(item);
		//item = QString::fromStdString(std::to_string(i) + " : " + std::to_string(vers[i]));
		//this->ui.versatilityList->addItem(item);
	
	//}
}