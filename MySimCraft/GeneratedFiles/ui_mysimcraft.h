/********************************************************************************
** Form generated from reading UI file 'mysimcraft.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYSIMCRAFT_H
#define UI_MYSIMCRAFT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MySimCraftClass
{
public:
    QWidget *centralWidget;
    QLineEdit *dps;
    QListWidget *intellectList;
    QListWidget *critList;
    QListWidget *hasteList;
    QListWidget *masteryList;
    QListWidget *versatilityList;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QPushButton *simButton;
    QCheckBox *plotCheckBox;
    QProgressBar *progressBar;
    QSpinBox *spinBox;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MySimCraftClass)
    {
        if (MySimCraftClass->objectName().isEmpty())
            MySimCraftClass->setObjectName(QStringLiteral("MySimCraftClass"));
        MySimCraftClass->resize(700, 641);
        centralWidget = new QWidget(MySimCraftClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        dps = new QLineEdit(centralWidget);
        dps->setObjectName(QStringLiteral("dps"));
        dps->setGeometry(QRect(30, 10, 113, 20));
        intellectList = new QListWidget(centralWidget);
        intellectList->setObjectName(QStringLiteral("intellectList"));
        intellectList->setGeometry(QRect(30, 60, 111, 501));
        critList = new QListWidget(centralWidget);
        critList->setObjectName(QStringLiteral("critList"));
        critList->setGeometry(QRect(160, 60, 111, 501));
        hasteList = new QListWidget(centralWidget);
        hasteList->setObjectName(QStringLiteral("hasteList"));
        hasteList->setGeometry(QRect(290, 60, 111, 501));
        masteryList = new QListWidget(centralWidget);
        masteryList->setObjectName(QStringLiteral("masteryList"));
        masteryList->setGeometry(QRect(420, 60, 111, 501));
        versatilityList = new QListWidget(centralWidget);
        versatilityList->setObjectName(QStringLiteral("versatilityList"));
        versatilityList->setGeometry(QRect(550, 60, 111, 501));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setEnabled(true);
        label->setGeometry(QRect(30, 40, 47, 13));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(160, 40, 47, 13));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(290, 40, 47, 13));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(420, 40, 47, 13));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(550, 40, 51, 16));
        simButton = new QPushButton(centralWidget);
        simButton->setObjectName(QStringLiteral("simButton"));
        simButton->setGeometry(QRect(160, 10, 75, 23));
        plotCheckBox = new QCheckBox(centralWidget);
        plotCheckBox->setObjectName(QStringLiteral("plotCheckBox"));
        plotCheckBox->setGeometry(QRect(590, 10, 70, 17));
        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(280, 10, 118, 23));
        progressBar->setValue(24);
        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(420, 10, 71, 22));
        spinBox->setMinimum(1);
        spinBox->setMaximum(1000000);
        spinBox->setSingleStep(1000);
        spinBox->setValue(15000);
        MySimCraftClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MySimCraftClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MySimCraftClass->setStatusBar(statusBar);

        retranslateUi(MySimCraftClass);

        QMetaObject::connectSlotsByName(MySimCraftClass);
    } // setupUi

    void retranslateUi(QMainWindow *MySimCraftClass)
    {
        MySimCraftClass->setWindowTitle(QApplication::translate("MySimCraftClass", "MySimCraft", 0));
        label->setText(QApplication::translate("MySimCraftClass", "Intellect:", 0));
        label_2->setText(QApplication::translate("MySimCraftClass", "Crit:", 0));
        label_3->setText(QApplication::translate("MySimCraftClass", "Haste:", 0));
        label_4->setText(QApplication::translate("MySimCraftClass", "Mastery:", 0));
        label_5->setText(QApplication::translate("MySimCraftClass", "Versatility:", 0));
        simButton->setText(QApplication::translate("MySimCraftClass", "Simulate", 0));
        plotCheckBox->setText(QApplication::translate("MySimCraftClass", "Plots", 0));
    } // retranslateUi

};

namespace Ui {
    class MySimCraftClass: public Ui_MySimCraftClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYSIMCRAFT_H
