#include "Priest.h"

void Priest::generateInsanity(float ammount) {
	insanity += ammount;
	if (insanity > 100) insanity = 100;
}

void Priest::decayInsanity(float castFinishTime) {
	while (LastInsanityDrainTickTime + 1 <= castFinishTime && VoidForm) {
		LastInsanityDrainTickTime++;
		insanity -= insanityDrainRate;
		if (insanity <= 0) {
			VoidForm = false; insanity = 0; return;
		}
		insanityDrainRate += 0.5;
	}
}

bool Priest::VoidFormStart() {
	if (insanity != 100 || VoidForm == true) return false;
	VoidForm = true;
	LastInsanityDrainTickTime = Time;
	insanityDrainRate = 10;
	Time += 1.5 / haste;
	decayInsanity( Time );
	nextCastDamage = 0;
	return true;
}

bool Priest::MindBlast() {
	if (Time < MindBlastCd) return false;
	Time += 1.5 / haste;
	decayInsanity(Time);
	MindBlastCd = Time + 9;
	generateInsanity(12);
	nextCastDamage = this->primaryStat * 3 * (1 + (rand() % 10000 < crit * 10000));
	return true;
}

bool Priest::VoidBolt() {
	if (VoidForm == false) return false;
	if (Time < VoidBoltCd) return false;
	Time += 1.5 / haste;
	decayInsanity(Time);
	VoidBoltCd = Time + 4.5;
	generateInsanity(16);
	nextCastDamage = this->primaryStat * 2.2 * (1 + (rand() % 10000 < crit * 10000));
	return true;
}

void Priest::MindFlay() {
	Time += 1.5 / haste;
	decayInsanity(Time);
	generateInsanity(4);
	nextCastDamage = this->primaryStat*2* (1 + (rand() % 10000 < crit * 10000));
}