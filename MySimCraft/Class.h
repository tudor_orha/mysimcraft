#pragma once
#include<vector>
#include<string>

class Class {
public:
	int iterations, targets;
	float Time;
	std::vector<int> talents;
	float primaryStat, crit, haste, mastery, versatility;
	double damage;
	float nextCastDamage;
public:
	Class() : iterations(0), targets(0), talents({ 0,0,0,0,0,0,0 }), primaryStat(0), crit(0), haste(0), mastery(0), versatility(0) {};

	Class(int iter, int tar, float time, std::vector<int> tal, float primaryStat, int crit, int haste, int mastery, int vers) :
		iterations(iter), targets(tar), Time(time), talents(tal), primaryStat(primaryStat),
		crit(0.05 + crit / 11000.0), haste(1 + haste / 10000.0), versatility(1 + vers / 13000.0)
	{};

	~Class() {};
	virtual double sim() = 0;
	std::vector<double> plotPrimary()
	{
		std::vector<double> plot;
		this->primaryStat -= 500 * 1.05;
		for (int i = 0; i <= 40; i++) {
			plot.push_back(this->sim());
			this->primaryStat += 25 * 1.05;
		}
		this->primaryStat -= 525 * 1.05;
		return plot;
	}
};