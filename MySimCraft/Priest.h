#pragma once
#include "Class.h"
#include <vector>

class Priest : public Class {
public:
	std::vector<float> results;
private:
	bool VoidForm;
	float insanity, insanityDrainRate, LastInsanityDrainTickTime;
	float MindBlastCd, VoidBoltCd;
public:
	Priest();
	Priest(int iter, int tar, float time, std::vector<int> tal, float intel, int crit, int haste, int mastery, int versatility);
	~Priest() {};
	void generateInsanity(float ammount);
	void decayInsanity(float castFinishTime);
	bool VoidFormStart();
	bool MindBlast();
	bool VoidBolt();
	void MindFlay();
	void iterationInit();
	double sim() override;
};
