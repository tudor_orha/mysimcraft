#include "Priest.h"

Priest::Priest() : Class()
{
}

Priest::Priest(int iter, int tar, float time, std::vector<int> tal, float intel, int crit, int haste, int mastery, int vers) :
	Class{ iter,tar,time,tal,intel,crit,haste,mastery,vers }
{
	this->mastery = (0.06 + float(mastery) / 14665);
}

void Priest::iterationInit() {
	damage = 0;
	Time = 0;
	MindBlastCd = 0;
	VoidBoltCd = 0;
	insanity = 0;
	VoidForm = false;
}