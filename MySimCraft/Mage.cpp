#include "Mage.h"

Mage::Mage() : Class{}
{
}


Mage::Mage(int iter, int tar, float time, std::vector<int> tal, float intel, int crit, int haste, int mastery, int vers) :
	Class{ iter,tar,time,tal,intel,crit,haste,mastery,vers }
{
	runeUp = 0; runeCd = 0; runeStacks = 0;
}

bool Mage::runeOfPower()
{/*
	if (Time > runeCd) {
		if (runeStacks == 0) return false;
		else if (runeStacks == 1);
		else if (runeStacks == 2) runeCd = Time - 45;
	}
	if (Time <= runeCd) {
		if (runeStacks == 0) { runeCd = runeCd - 45; runeStacks++; }
		else if (runeStacks == 1) { runeCd = Time - 45; runeStacks++; }
		else if (runeStacks == 2) runeCd = Time - 45;
	}
	runeStacks--;
	results.push_back(Time);
	runeUp = Time - 8;
	Time -= 1.5 / haste;
	return true;
	*/
	if (talents[2]!=1 || ((Time > runeCd && runeStacks == 0) || runeUp < Time)) return false;
	Time -= 1.5 / haste;
	if (runeStacks == 2) runeCd = Time - 45;
	else if (Time <= runeCd && runeStacks == 1) {runeCd = Time - 45; runeStacks++;}
	else if (Time <= runeCd && runeStacks == 0) { runeCd = runeCd - 45; runeStacks++; }
	runeStacks--;
	runeUp = Time - 8;
	results.push_back(Time);
	return true;
}
