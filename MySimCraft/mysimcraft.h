#ifndef MYSIMCRAFT_H
#define MYSIMCRAFT_H

#include <QtWidgets/QMainWindow>
#include "ui_mysimcraft.h"
#include "Priest.h"
#include "FireMage.h"

class MySimCraft : public QMainWindow
{
	Q_OBJECT

public:
	MySimCraft(QWidget *parent = 0);
	~MySimCraft() {}
	Class *c;
	int iterations;
	void simulate();
	void plot();

private:
	Ui::MySimCraftClass ui;

public slots:
	void run();
	
};

#endif // MYSIMCRAFT_H
