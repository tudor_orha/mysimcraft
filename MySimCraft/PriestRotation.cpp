#include "Priest.h"
#include <ctime>
#include <stdlib.h>


double Priest::sim()
{
	int iters = iterations;
	float itime = Time;
	double dps = 0;
	srand(time(NULL));
	while (iters > 0) {
		iterationInit();
		while (Time < itime)
		{
			if (VoidFormStart());
			else if (MindBlast());
			else if (VoidBolt());
			else MindFlay();
			if (VoidForm) damage += nextCastDamage*1.2;
			else damage += nextCastDamage*1;
		}
		dps += damage / Time;
		iters--;
		// pain : 45 sp , 315sp over 14 sec
		// vampiric: 6 + 522 over 18secs
	}
	return (dps / iterations)*versatility;
}

/*
std::vector<double> FireMage::plotIntel()
{
	std::vector<double> plot;
	this->primaryStat -= 500 * 1.05;
	for (int i = 0; i <= 40; i++) {
		plot.push_back(this->sim());
		this->primaryStat += 25 * 1.05;
	}
	this->primaryStat -= 525 * 1.05;
	return plot;
}

//crit(0.05+float(crit)/11000),haste(1+float(haste)/10000),mastery(0),versatility(1+float(vers)/13000)

std::vector<double> FireMage::plotCrit()
{
	std::vector<double> plot;
	this->crit -= 500.0 / 11000;
	for (int i = 0; i <= 40; i++) {
		plot.push_back(this->sim());
		this->crit += 25.0 / 11000;
	}
	crit -= 525.0 / 11000;
	return plot;
}

std::vector<double> FireMage::plotHaste()
{
	std::vector<double> plot;
	this->haste -= 500.0 / 10000;
	for (int i = 0; i <= 40; i++) {
		plot.push_back(this->sim());
		this->haste += 25.0 / 10000;
	}
	haste -= 525.0 / 10000;
	return plot;
}

std::vector<double> FireMage::plotMastery()
{
	std::vector<double> plot;
	this->mastery -= 500.0 / 14665;
	for (int i = 0; i <= 40; i++) {
		plot.push_back(this->sim());
		this->mastery += 25.0 / 14665;
	}
	mastery -= 525.0 / 14665;
	return plot;
}

std::vector<double> FireMage::plotVersatility()
{
	std::vector<double> plot;
	this->versatility -= 500.0 / 13000;
	for (int i = 0; i <= 40; i++) {
		plot.push_back(this->sim());
		this->versatility += 25.0 / 13000;
	}
	versatility -= 525.0 / 13000;
	return plot;
}
*/
