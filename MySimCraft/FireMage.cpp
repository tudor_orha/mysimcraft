#include "FireMage.h"
#include <stdlib.h>

FireMage::FireMage() : Mage()
{
}

FireMage::FireMage(int iter, int tar, float time, std::vector<int> tal, float intel, int crit, int haste, int mastery, int vers) :
	Mage{ iter,tar,time,tal,intel,crit,haste,mastery,vers }
{
	this->crit += 0.20;
	this->mastery = (0.06 + float(mastery)/14665);
	pyroProc = 0; EnhancedPyrotechnics = 0; fireBlastCd = 0; dragonsBreathCd = 0; livingBombCd = 0; combustCd = 0; combustUp = 0;
}

void FireMage::fireball()
{
	bool critt;
	this->Time -= 2.25 / this->haste;
	//FireStarter
	if((talents[0]==2 && false) || Time > combustUp) critt = 1;
	else critt = rand() % 10000 < crit * 10000 * 1.1 * (1 + EnhancedPyrotechnics*0.1);
	//if (runeUp < Time) mult *= 1.5;
	//Unstable Magic
	//if (talents[5] == 1) this->damage += this->intellect*0.9*(1 + critt)*targets * (rand() % 4 == 0);
	damage += (1+primaryStat*1.8)*(1 + critt) * (1 + mastery*targets);
	if (critt && pyroProc<2) pyroProc++;
	if (critt != true && pyroProc != 2) pyroProc = 0;
	if (critt) EnhancedPyrotechnics = 0;
	else EnhancedPyrotechnics++;
}

void FireMage::pyroblast()
{
	bool critt;
	//if (runeUp < Time) mult *= 1.5;
	if (pyroProc == 2)
	{
		Time -= 1.5 / haste;
		if (Time > combustUp) critt = 1;
		else critt = rand() % 10000 < crit * 10000 * 1.1;
		damage += primaryStat*4.2*(1 + critt) * (1 + mastery*targets * 2);
		pyroProc = 0;
		if (critt) pyroProc++;
	}
	else
	{
		Time -= 4.5 / haste;
		if (Time > combustUp) critt = 1;
		else critt = rand() % 10000 < crit * 10000 * 1.1;
		damage += primaryStat*4.2*(1 + critt) * (1 + mastery*targets);
		if (critt) pyroProc++;
		else pyroProc = 0;
	}
	

}


bool FireMage::fireBlast()
{
	if (Time > fireBlastCd) return false;
	fireBlastCd = Time - 12.0 / haste;
	damage += primaryStat*1.4*2 * (1 + mastery*targets);
	if (pyroProc < 2) pyroProc++;
	return true;
}

bool FireMage::dragonsBreath()
{
	if (Time > dragonsBreathCd) return false;
	dragonsBreathCd = Time - 20;
	damage += this->primaryStat*targets*2.5 *(1 + (rand() % 10000 < crit * 10000));
	Time -= 1.5 / haste;
	return true;
}

bool FireMage::livingBomb()
{
	if (Time > livingBombCd || talents[5] != 0) return false;
	livingBombCd = Time - float(12) / haste;
	damage += this->primaryStat*targets*(1 + 0.8*targets) *(1 + (rand() % 10000 < crit * 10000));
	Time -= 1.5 / haste;
	return true;
}

bool FireMage::combustion()
{
	if (Time > combustCd) return false;
	combustCd = Time - 120;
	combustUp = Time - 10;
	return true;
}

double FireMage::sim()
{
	//int iters = iterations;
	float itime = Time;
	double dps = 0;
	//while (iters > 0) {
		damage = 0;
		fireBlastCd = Time;
		livingBombCd = Time;
		dragonsBreathCd = Time;
		combustCd = Time;
		combustUp = Time;
		EnhancedPyrotechnics = 0;
		pyroProc = 0;
		runeStacks = 2;
		runeCd = Time;
		runeUp = Time;
		while (Time > 0)
		{
			if (false);
			//if (runeOfPower());
			//else if (combustion());
			//else if (pyroProc==1 && fireBlast());
			//else if (dragonsBreath());
			//else if (pyroProc == 2) pyroblast();
			//else if (livingBomb());
			else if (true) fireball();
			else Time--;
		}
		Time = itime;
		dps += damage / Time;
		//iters--;
	//}
	if (talents[2] == 2) dps *= 1.12;
	//return (dps / iterations)*versatility;
	return dps*versatility;
}

/*
//crit(0.05+float(crit)/11000),haste(1+float(haste)/10000),mastery(0),versatility(1+float(vers)/13000)

std::vector<double> FireMage::plotCrit()
{
	std::vector<double> plot;
	this->crit -= 500.0/11000;
	for (int i = 0; i <= 40; i++) {
		plot.push_back(this->sim());
		this->crit += 25.0/11000;
	}
	crit -= 525.0/11000;
	return plot;
}

std::vector<double> FireMage::plotHaste()
{
	std::vector<double> plot;
	this->haste -= 500.0/10000;
	for (int i = 0; i <= 40; i++) {
		plot.push_back(this->sim());
		this->haste += 25.0/10000;
	}
	haste -= 525.0/10000;
	return plot;
}

std::vector<double> FireMage::plotMastery()
{
	std::vector<double> plot;
	this->mastery -= 500.0/14665;
	for (int i = 0; i <= 40; i++) {
		plot.push_back(this->sim());
		this->mastery += 25.0/14665;
	}
	mastery -= 525.0/14665;
	return plot;
}

std::vector<double> FireMage::plotVersatility()
{
	std::vector<double> plot;
	this->versatility -= 500.0/13000;
	for (int i = 0; i <= 40; i++) {
		plot.push_back(this->sim());
		this->versatility += 25.0/13000;
	}
	versatility -= 525.0/13000;
	return plot;
}
*/
